
resource "google_project_service" "svc-gke" {
  provider = google-beta
  service = "container.googleapis.com"
}

resource "google_project_service" "svc-cloud-monitoring" {
  provider = google-beta
  service = "monitoring.googleapis.com"
}
    
resource "google_project_service" "svc-cloud-cloud-trace" {
  provider = google-beta
  service = "cloudtrace.googleapis.com"
}

resource "google_project_service" "svc-cloud-logging" {
  provider = google-beta
  service = "logging.googleapis.com"
}

resource "google_project_service" "svc-meshca" {
  provider = google-beta
  service = "meshca.googleapis.com"
}

resource "google_project_service" "svc-mesh-telemetry" {
  provider = google-beta
  service = "meshtelemetry.googleapis.com"
}

resource "google_project_service" "svc-meshconfig" {
  provider = google-beta
  service = "meshconfig.googleapis.com"
}

resource "google_project_service" "svc-iamcreds" {
  provider = google-beta
  service = "iamcredentials.googleapis.com"
}

resource "google_project_service" "svc-gkeconnect" {
  provider = google-beta
  service = "gkeconnect.googleapis.com"
}

resource "google_project_service" "svc-gkehub" {
  provider = google-beta
  service = "gkehub.googleapis.com"
}

resource "google_project_service" "svc-orgpolicy" {
  provider = google-beta
  service = "orgpolicy.googleapis.com"
}

resource "google_project_service" "svc-mci" {
  provider = google-beta
  service = "multiclusteringress.googleapis.com"
}

resource "google_project_service" "svc-mc-svc-disc" {
  provider = google-beta
  service = "multiclusterservicediscovery.googleapis.com"
}
