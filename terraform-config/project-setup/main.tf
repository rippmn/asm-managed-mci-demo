provider "google-beta" {
  project     = var.project_id
  region      = var.region-1
  zone   = var.zone-1
}


data "google_project" "project" {
    provider = google-beta
    project_id = var.project_id
}
