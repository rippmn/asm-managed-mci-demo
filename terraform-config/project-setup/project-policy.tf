resource "google_project_organization_policy" "private_cluster_vpc_peering" {
  provider = google-beta
  project = var.project_id
  constraint = "compute.restrictVpcPeering"

  list_policy {
    allow {
      all = true
    }
  }
}
