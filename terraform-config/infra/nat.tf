resource "google_compute_firewall" "ssh" {
  provider = google-beta
  name    = "jump-ssh"
  network = google_compute_network.vpc_network.name

  direction = "INGRESS"


  priority = "1000"

  allow {
    protocol = "tcp"
    ports = ["22"]
  }

  source_ranges = [ "0.0.0.0/0"]
}

resource "google_compute_router" "router-central" {
  provider = google-beta
  name    = "central-router"
  network = google_compute_network.vpc_network.name
  region  = var.region-1
  project = var.project_id
}

resource "google_compute_router_nat" "nat-central" {
  provider = google-beta
  name                               = "central-router-nat"
  router                             = google_compute_router.router-central.name
  region                             = google_compute_router.router-central.region
  project 			     = var.project_id
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

}

resource "google_compute_router" "router-west" {
  provider = google-beta
  name    = "west-router"
  network = google_compute_network.vpc_network.name
  region  = var.region-2
  project = var.project_id
}

resource "google_compute_router_nat" "nat-west" {
  provider = google-beta
  name                               = "west-router-nat"
  router                             = google_compute_router.router-west.name
  region                             = google_compute_router.router-west.region
  project 			     = var.project_id
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

}
