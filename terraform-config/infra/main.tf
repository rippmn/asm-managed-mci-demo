provider "google-beta" {
  project     = var.project_id
}


data "google_project" "project" {
    provider = google-beta
    project_id = var.project_id
}

