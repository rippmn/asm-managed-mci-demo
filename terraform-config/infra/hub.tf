resource "google_gke_hub_membership" "asm_central" {
  provider = google-beta
  membership_id = "asm-central"
  project = var.project_id
  endpoint {
    gke_cluster {
      resource_link = "//container.googleapis.com/${google_container_cluster.demo_cluster_central.id}"
    }
  }
  authority {
    issuer = "https://container.googleapis.com/v1/${google_container_cluster.demo_cluster_central.id}"
  }
}

resource "google_gke_hub_feature" "mci-hub-feature" {
  provider = google-beta
  name = "multiclusteringress"
  location = "global"
  spec {
    multiclusteringress {
      config_membership = google_gke_hub_membership.asm_central.id
    }
  }
  
} 

resource "google_gke_hub_membership" "asm_west" {
  provider = google-beta
  membership_id = "asm-west"
  project = var.project_id
  endpoint {
    gke_cluster {
      resource_link = "//container.googleapis.com/${google_container_cluster.demo_cluster_west.id}"
    }
  }
  authority {
    issuer = "https://container.googleapis.com/v1/${google_container_cluster.demo_cluster_west.id}"
  }
}

resource "time_sleep" "wait_b4_asm_enable" {
  depends_on = [google_gke_hub_membership.asm_central,google_gke_hub_membership.asm_west]

  create_duration = "30s"
}

resource "google_gke_hub_feature" "asm_feature" {
  provider = google-beta

  name = "servicemesh"
  location = "global"
  depends_on = [time_sleep.wait_b4_asm_enable]
}

resource "google_gke_hub_feature_membership" "asm_central_enable" {
  location = "global"
  feature = google_gke_hub_feature.asm_feature.name
  membership = google_gke_hub_membership.asm_central.membership_id
  mesh {
    management = "MANAGEMENT_AUTOMATIC"
  }
  provider = google-beta
}

resource "google_gke_hub_feature_membership" "asm_west_enable" {
  location = "global"
  feature = google_gke_hub_feature.asm_feature.name
  membership = google_gke_hub_membership.asm_west.membership_id
  mesh {
    management = "MANAGEMENT_AUTOMATIC"
  }
  provider = google-beta
}
