resource "google_service_account" "demo_sa" {
  provider = google-beta
  account_id   = "demo-sa"
  display_name = "demo Service Account"
}

resource "google_project_iam_member" "demo_sa_editor" {
  provider = google-beta
  project = var.project_id
  role    = "roles/editor"
  member =   format("%s:%s", "serviceAccount", google_service_account.demo_sa.email )
}

resource "google_project_iam_member" "demo_sa_cluster_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/container.admin"
  member =   format("%s:%s", "serviceAccount", google_service_account.demo_sa.email )
}


resource "google_service_account" "demo_node_sa" {
  provider = google-beta
  account_id   = "demo-node-sa"
  display_name = "demo Service Account"
}

resource "google_project_iam_member" "demo_node_sa_perm" {
  provider = google-beta
  project = var.project_id
  role    = "roles/container.nodeServiceAccount"
  member =   format("%s:%s", "serviceAccount", google_service_account.demo_node_sa.email )
}

