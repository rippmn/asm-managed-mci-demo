resource "google_container_cluster" "demo_cluster_west" {
  provider = google-beta
  name               = "demo-cluster-west"
  location           = var.zone-2

  initial_node_count = 2

  network = google_compute_network.vpc_network.name

  subnetwork = google_compute_subnetwork.demo_subnet_west.name

  ip_allocation_policy{
    //this block turns on vpc native mode you can specify specific subnets or cidrs for the node and services subnet, or accept the defaults here
    //note secondary ranges are returned in the order created on the subnet so this might seem a little wonky
    cluster_secondary_range_name = google_compute_subnetwork.demo_subnet_west.secondary_ip_range[0].range_name
    services_secondary_range_name = google_compute_subnetwork.demo_subnet_west.secondary_ip_range[1].range_name
  }

  node_config {
    service_account = google_service_account.demo_node_sa.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    image_type = "cos_containerd"
    machine_type = "e2-standard-4"
    disk_size_gb = "100"

    metadata = {
      disable-legacy-endpoints = "true" 
    }
   
    shielded_instance_config{
      enable_secure_boot = "true"
    }

    tags = [ "asm-node-w4"]
  }

  private_cluster_config {
    enable_private_nodes = "true"
    enable_private_endpoint = "true"
    master_ipv4_cidr_block = "10.114.0.16/28"
  
    master_global_access_config {
      enabled = "true"
    }

  }

  master_authorized_networks_config {
    cidr_blocks {
      cidr_block = format("%s/32",google_compute_instance.jump.network_interface.0.network_ip)
    }
    cidr_blocks {
      cidr_block = google_compute_subnetwork.demo_subnet_west.ip_cidr_range
    }
    cidr_blocks {
      cidr_block = google_compute_subnetwork.demo_subnet_central.secondary_ip_range[0].ip_cidr_range
    }
  }

  workload_identity_config {
    workload_pool = "${var.project_id}.svc.id.goog"
  }

  resource_labels = { 
    "mesh_id" : "proj-${data.google_project.project.number}" 
  }

}


#########
