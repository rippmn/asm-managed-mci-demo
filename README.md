**Purpose**

This project is a set of terraform and Kubernetes configurations to demonstrate the usage of Anthos Service Mesh as a multi cluster mesh in conjunction with Google Multi Cluster Ingress. At the end of these steps you will have 2 zonal clusters running in 2 separate regions (default of us-central1 and us-west4) with an application with front-end and backend services deployed on both clusters with multi cluster service discovery and a single global ingress pointing to the multiple regions.


**Preconditions/Assumptions/Notes**
* An existing GCP Project to use as sandbox
* the design of the inffrastructure expects the project to be isolated and leverages an project local VPC. Additionally, the ip space in the project is not constrianed by other usages as there are hardcoded ip ranges. If this is not the case ensure that the network config and gke configurations of the master ip ranges reflect the ip ranges that will work for you.
* The terraform config creates a network, subnetworks including secondary ranges and necessary firewall rules. There are IP ranges defined in the config that may need adjust
* User and/or Terraform SA is project editor 
* The clusters created are private with private endpoints - a jump-host is stood up to access the clusters control plane
* Example calls made to the application are done using curl passing in the host-domain wai.rippmn.com as the "Host" header along with the ip address of the gateway service (either regional IP or the MCI mapped IP). To test in a web browser this host-domain must resolve to the IP address of the gateway service. Alternatively you can update the instruction to use a host-domain where you can setup DNS to map to the IP address(es) of the gateways. The host-domain would need to be updated in the demo-app/wai-web/whereami-gw.yaml file.


**Project Layout**

- terraform-config
  - project setup - terraform config  to enable apis and configure project specific policies **Must be run before infra can be built**
  - infra - terraform config to create networks, firewalls, jump-host, and clusters clusters and other resources. 
- demo-app - sample application K8s resources
  - wai-api - sample application backend workload 
  -wai-web - sample application frontend workload
  -destrule - destination rules to enable regional failover and local region load balancing (vs cross region) for frontend and backend
  -mci - GCP MultiClusterIngress setup for in cluster ASM ingress Gateways 


**Steps to perform**

Note to setup the project use the terraform config in the project setup directory. This is a one time requirement for the new project and should be run separtely from the terraform config in the infra directory. This enables error free use of terraform destroy on the infr configuration without transitive errors that usually occur when disabling cloud services.  If this is a new project it can take up to 5 mins for the compute api to be ready for the setup config, so the application of the terraform config in the infra directory may fail at the network setup phase.

# Demo Steps

## Project & Infra Setup

The Terraform config includes the following:
authorized networks setup here - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#private-clusters-authorized-network

and the firewall rule requirements here (modifed to use a explictly set tag) - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#create_firewall_rule

and the global access as described here - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#enable_control_plane_global_access

and register the clusters to gke hub and enablement of multicluster ingress feature

to interact with the clusters (as they have private endpoints) you will need to ssh into the included jump host

In the terraform-config/project-setup directory
* Update the Terraform terraform.tfvars file for you particular project. 
* Initialize terraform 
* apply the configuration

In the terraform-config/infra directory
* Update the Terraform terraform.tfvars file for you particular project. 
* If you want to use different regions make adjustments in network.tf and subnet references in gce.tf and gke*.tf as well as later references in these instructions. 
* Initialize terraform 
* apply the configuration

Copy the Kubernetes Resource Configs in the k8s-config directory to the jump host using the following commands from the root project directory (note these are Linux/Mac instructions, for windows where tar is not available substitute zip) 
```
tar cvf asm-mci-demo.tar.gz k8s-config/
gcloud compute scp asm-mci-demo.tar.gz jump-host:. --zone us-central1-f
rm asm-mci-demo.tar.gz
```


ssh into jump-host 
```
gcloud compute ssh jump-host --zone us-central1-f
```

Use GCP GKE Auth Plugin with kubectl
(note if you disconnect from SSH session you will need to run the env var setting USE_GKE_GCLOUD_AUTH_PLUGIN on reconection)
```
export USE_GKE_GCLOUD_AUTH_PLUGIN=True
```

Unpack demo app config
```
tar xvf asm-mci-demo.tar.gz
```

Setup Env Vars
```
export PROJECT_ID=$(gcloud config get-value project)
export FLEET_PROJECT_ID=${PROJECT_ID}
export PROJECT_1=${PROJECT_ID}
export PROJECT_2=${PROJECT_ID}
export LOCATION_1=us-central1-f
export CLUSTER_1=demo-cluster-central
export CTX_1="gke_${PROJECT_1}_${LOCATION_1}_${CLUSTER_1}"
export LOCATION_2=us-west4-c
export CLUSTER_2=demo-cluster-west
export CTX_2="gke_${PROJECT_2}_${LOCATION_2}_${CLUSTER_2}"
alias k=kubectl
```

Get cluster credentials
```
gcloud container clusters get-credentials demo-cluster-central --zone us-central1-f
gcloud container clusters get-credentials demo-cluster-west --zone us-west4-c
```

check to see if ASM is ready (wait ~5-10 mins)
```
gcloud container fleet mesh describe
```

You should see a message similar to the following for each cluster:
```
    servicemesh:
      controlPlaneManagement:
        details:
        - code: REVISION_READY
          details: 'Ready: asm-managed'
        state: ACTIVE
      dataPlaneManagement:
        details:
        - code: OK
          details: Service is running.
        state: ACTIVE
    state:
      code: OK
      description: 'Revision(s) ready for use: asm-managed.'
      updateTime: '2022-12-05T21:56:08.905947602Z'
```

or via kubectl (set context to each cluster)
```
kubectl describe controlplanerevision asm-managed -n istio-system --context $CTX_1
kubectl describe controlplanerevision asm-managed -n istio-system --context $CTX_2
```
A completed deployment will display a message as follows:
```
Status:
  Conditions:
    Last Transition Time:  2022-12-05T21:55:36Z
    Message:               The provisioning process has completed successfully
    Reason:                Provisioned
    Status:                True
    Type:                  Reconciled
    Last Transition Time:  2022-12-05T21:55:36Z
    Message:               Provisioning has finished
    Reason:                ProvisioningFinished
    Status:                True
    Type:                  ProvisioningFinished
    Last Transition Time:  2022-12-05T21:55:36Z
    Message:               Provisioning has not stalled
```

**Optional Multi Cluster Service Sample App Deployment**

The following steps demonstrate cross cluster service load balancing. If you want to proceed to the main demo go to the [Application Deployment and Configuration](#application-deployment-and-configuration) section below.  

Enable sidecar injection and deploy sample

```
export LBL=$(k get ControlPlaneRevision -n istio-system -o jsonpath={.items[0].metadata.name})

for CTX in ${CTX_1} ${CTX_2}
do
    kubectl create --context=${CTX} namespace sample
    kubectl label --context=${CTX} namespace sample \
        istio-injection- istio.io/rev=$LBL --overwrite
    kubectl --context ${CTX} -n sample run --image=gcr.io/google-samples/whereami:v1.2.6 --expose --port 8080 whereami 
done
```

**Testing MultiCluster Service Discovery**

Deploy nginx to the sample namespace and the remote exec in to curl the whereami.sample:8080/hello endpoint. You shoud see the servicing deployments change from central to west (note here we are using the central clusster $CTX_1, but this would work from West as well)

Run Nginx image
```
k run nginx -n sample --image nginx --context $CTX_1
```
SSH into the image
```
k exec -it nginx -n sample --context $CTX_1 -- /bin/bash
```
Curl whereami sample (do multiple times to observer the returned zone change)
```
curl whereami.sample:8080/hello
```

**Sample Application CLEANUP**

run below (note it also cleans up any nginx pods)

```
for CTX in ${CTX_1} ${CTX_2}
do
    k delete svc whereami -n sample --context ${CTX}
    k delete po whereami -n sample --context ${CTX}
    k delete po nginx -n sample --context ${CTX}
    k delete ns sample --context ${CTX}
done
```

## Application Deployment and Configuration

**deploy whereami api**

Create the API namespace and deploy the whereami app as a backend api for the web tier to use.
This should be performed from the root folder of the unpacked tar file "k8s-config"
```
cd ~/k8s-config
export LBL=$(k get ControlPlaneRevision -n istio-system -o jsonpath={.items[0].metadata.name})
sed -i "s/LBL/${LBL}/g" demo-app/wai-api-ns.yaml
for CTX in ${CTX_1} ${CTX_2}
do
    k apply -f demo-app/wai-api-ns.yaml --context=${CTX} 
    k apply -f demo-app/wai-api --context=${CTX} 
done
```

To disable cross region service load balancing for the api service, setup cross region as failover using the destination rules for the wai-svc in the destrules directory
```
k apply -f demo-app/destrule/wai-svc-dest-rule-central.yaml --context ${CTX_1}
k apply -f demo-app/destrule/wai-svc-dest-rule-west.yaml --context ${CTX_2}
```

**INSTALL GATEWAYS**
See https://cloud.google.com/service-mesh/docs/gateways

Note - since using managed control plane get the sample ingress gateway config
```
cd ~
git clone https://github.com/GoogleCloudPlatform/anthos-service-mesh-packages.git

```
Now setup the gateways (note using the ControlPlaneRevision installed in istio-system namespace to determine the label to use)
```
export LBL=$(k get ControlPlaneRevision -n istio-system -o jsonpath={.items[0].metadata.name})
for CTX in ${CTX_1} ${CTX_2}
do
    kubectl create --context=${CTX} namespace asm-gateway
    kubectl label --context=${CTX} namespace asm-gateway \
        istio-injection- istio.io/rev=$LBL --overwrite
    kubectl apply -n asm-gateway -f anthos-service-mesh-packages/samples/gateways/istio-ingressgateway --context ${CTX}
done
```

**deploy whereami web and setup ingress gateway**
```
cd ~/k8s-config
export LBL=$(k get ControlPlaneRevision -n istio-system -o jsonpath={.items[0].metadata.name})
sed -i "s/LBL/${LBL}/g" demo-app/wai-web-ns.yaml
for CTX in ${CTX_1} ${CTX_2}
do
    k apply -f demo-app/wai-web-ns.yaml --context=${CTX} 
    k apply -f demo-app/wai-web --context=${CTX} 
done
```

**Test gateway deployment**

get the ip address of the gateway services then
```
export GW_CENTRAL_IP=$(kubectl get service -n asm-gateway --context $CTX_1 -o jsonpath={.items[0].status.loadBalancer.ingress[0].ip})
export GW_WEST_IP=$(kubectl get service -n asm-gateway --context $CTX_2 -o jsonpath={.items[0].status.loadBalancer.ingress[0].ip})

curl -HHost:wai.rippmn.com $GW_CENTRAL_IP
curl -HHost:wai.rippmn.com $GW_WEST_IP
```
On repeated calls to the same regional gateway you should see the whereami service respond from both regions (us-central1 and us-west4)

**Enabled Locality only LB for whereami web tier**
To enable LB only among the region local whereami instances apply the destination rules in the destrules folder

```
cd ~
k apply -f k8s-config/demo-app/destrule/whereami-dest-rule-central.yaml --context ${CTX_1}
k apply -f k8s-config/demo-app/destrule/whereami-dest-rule-west.yaml --context ${CTX_2}
```

Repeated calls to curl with a regional GW service IP you should now only see responses from the local whereami instances
```
curl -HHost:wai.rippmn.com $GW_CENTRAL_IP
curl -HHost:wai.rippmn.com $GW_WEST_IP
```

**Demo Regional Failover** 
Pick a region and scale the wai-svc replicas to 0 (in this case using central)
```
k scale deploy wai-svc --replicas 0 -n wai-api --context $CTX_1
```
Wait for the replicas to terminate and then execute the curl again. You should notice that the backend response now comes from the west region.
```
curl -HHost:wai.rippmn.com $GW_CENTRAL_IP
```
Scale the wai-svc replicas back to 3 (in central in this example)
```
k scale deploy wai-svc --replicas 3 -n wai-api --context $CTX_1
```
Note: after restoration there can be a delay of upto a minute before the wai-svc failover ends as the destination rules set an ejection time of one minute. 

**Multi Cluster Ingress**
Delete the regional gateway services as these regional addresses will be replaced with a global address (this needs to be run from the root directory)
```
cd ~
k delete -f anthos-service-mesh-packages/samples/gateways/istio-ingressgateway/service.yaml -n asm-gateway --context $CTX_1
k delete -f anthos-service-mesh-packages/samples/gateways/istio-ingressgateway/service.yaml -n asm-gateway --context $CTX_2
```

Deploy the multi cluster service

(Note the health check backend needs to be deployed in all clusters)
```
k apply -f k8s-config/mci/ingress-health-backend.yaml --context $CTX_1
k apply -f k8s-config/mci/ingress-health-backend.yaml --context $CTX_2
k apply -f k8s-config/mci/ingress-mcs.yaml --context $CTX_1
k apply -f k8s-config/mci/ingress-mci.yaml --context $CTX_1
```

You will need to wait a minute or so for the global IP address to be assigned. Once the process completes the following will return the IP address.
```
kubectl get multiclusteringress -n asm-gateway --context $CTX_1 -o jsonpath={.items[0].status.VIP}
```

Once the IP address has been setup execute the following to use the Multi Cluster Service.

```
export MCI_IP=$(kubectl get multiclusteringress -n asm-gateway --context $CTX_1 -o jsonpath={.items[0].status.VIP})
curl -HHost:wai.rippmn.com ${MCI_IP}
```

You can also try out changing the region of the jump-host and reapply the terraform config (or add a new GCE instance in the other region ) to see the location based routing of the Multi Cluster Ingress. (Note you will need to capture the MCI_IP address as the new instance will no longer have the local config done to this point.)


**Gateway and Example NS  CLEANUP**

run below 

```
for CTX in ${CTX_1} ${CTX_2}
do
    k delete ns wai-web --context ${CTX}
    k delete ns wai-api --context ${CTX}
    k delete ns asm-gateway --context ${CTX}
done
```
